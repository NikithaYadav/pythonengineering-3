from pprint import pprint

"""
Summary
    Define:             d = {
                          <key>: <value>,
                          <key>: <value>
                        }

    Add and Update:     dictionary[<key>] = <value>

    Loop:               dict or dict.keys()
                        dict.items()
                        dict.values()

    [Limitation:         Keys should only be immutable types]
"""

"""
    dictionaries are:
        mutable     
        unordered (upto python 3.5)
        duplicate keys are not allowed - updating will overwrite the previous value
        indexing is not possible (due to Hashing)
"""

# Define ip address table
ip_address = {
    'localhost': '127.0.0.1',
    'subnet': '10.197.0.0',  # 10.197.0.99
    'default_gw': '10.197.0.1',
    'dns': ('10.197.0.2', '10.197.0.20'),
    'assigned': ['10.197.0.4'],  # expand or shrink based on people allotment
}
pprint(ip_address)

# # Immutable keys example:
# ip_address_immutable = {
#     'localhost': '127.0.0.1',
#     'subnet': '10.197.0.0',             # 10.197.0.100
#     'default_gw': '10.197.0.1',
#     'dns': '10.197.0.2',
#     'assigned': ['10.197.0.4'],         # expand or shrink based on people allotment
#     # [1, 3]: 'First two odd numbers'
#     (1, 3): 'First two odd numbers',    # tuple key
#     22 / 7: 'pi value'                  # no restrictions for values
# }
# pprint(ip_address[0])   #   KeyError: 0
list_of_students = [('chi', 25), ('nik', 25)]
# student_table = dict(list_of_students)
# student_table = dict(nik=25, chi=25)
# pprint(student_table)


# # Lists are indexed
# l = list(range(5))
# print(l[-1])
#
# for i in l:
#     if i == 3:
#         print('3 is found')
#
#
# d = {
#     1: 'one',
#     2: 'two',
#     3: 'three',
# }
#
# for i in range(10):
#     # iterate 10 times
#     pass
#
# if 3 in d:
#     print('3 is found')
# else:
#     print('3 is NOT found')
#
# print(f"1's value is {d[1]}")   # KeyError if key is not found

# Add item
ip_address['vlan_ip'] = '10.197.0.100'

# Update item
ip_address['dns'] = '10.197.0.3'
# pprint(ip_address)

# Update a key with multiple value
assigned_ips = ip_address['assigned']
assigned_ips.append('10.197.0.5')

# Add second
ip_address['assigned'].append('10.197.0.6')
pprint(ip_address)

# Deprecated way to add multiple items
ip_address[3.14] = 'pi'
ip_address[100] = '10 squared'
ip_address['200'] = '200 in str'

# Recommended approach to add multiple
# key-value pairs: dict.update()
new_ips = {
    'hrs_ip_range': ['10.197.0.10', '10.197.0.11'],
    'admin_ip_range': ['10.197.0.20', '10.197.0.21'],
    'default_gw': '10.197.0.99'
}
ip_address.update(new_ips)  # update(dict)
pprint(ip_address)

# Looping dictionaries
# 1. loop dictionary directly - keys
for item in ip_address:  # deprecated: ip_address.keys()
    print(item)

# When want to list the keys explicitly
pprint(list(ip_address.keys()))

# 2. loop dictionary with values - dict.values()

for value in ip_address.values():
    print(value)

# Check if any part of org has more than 1 ips
found = False
for value in ip_address.values():
    if type(value) is list and len(value) > 10:
        found = True
print('Found:', found)

pprint(ip_address)

# 3. loop dictionary with items - dict.items() - list of tuples (key, value)
for key, value in ip_address.items():
    if type(value) == list and len(value) > 1:
        print(f'The org unit "{key}" is having "{len(value)}" ips: {value}')
