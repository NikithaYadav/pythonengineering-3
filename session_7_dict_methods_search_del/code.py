from pprint import pprint

"""
    Searching dictionary
      'in' keyword

    Common dictionary methods
        1. dict.get(key, [default])   - returns if item exists, else None
        2. dict.items()               - returns keys and values as list of tuples
        3. dict.keys()                - returns keys as list
        4. dict.values()              - returns values as list
        5. dict.pop(key)              - removes the item from dict and returns value
        6. dict.update(dict)          - Updates a dictionary to the list
"""
# The "in" keyword - search for an item in a given iterable
# (list, tuple, dict, set, string)
ip_address = {
    'localhost': '127.0.0.1',
    'subnet': '10.197.0.0',  # 10.197.0.99
    'default_gw': '10.197.0.1',
    'dns': ('10.197.0.2', '10.197.0.20'),  # tuple - ips cannot be changed for dns server
    # 'assigned': ['10.197.0.4'],
}

print('a' in 'apple')
print('app' in 'apple')  # substring
print('h' in 'apple')
print(3.14 in [5, 6, 7, 8])
print(0 in {0, 5, 6, 7, 8})
pprint(ip_address)
pprint(list(ip_address))  # typecasting will fetch all the keys

# for k in ip_address:
#     print(k)

print('dns' in ip_address)
print('admin' in ip_address)
print('vlan' not in ip_address)

a = 10  # load from db query
print(type(a) in (int, float))

if type(a) == int or type(a) == float or type(a) == dict:
    print(True)

# What are uses of python "in" keyword?
# 1. to fetch items in for loop
# 2. To search for an element in list, set, tuple, dict, str
# For example, in is used to find substrings in a string

for value in ip_address.values():
    if type(value) in (list, tuple) and len(value) > 1:
        found = True

""" Common methods in dictionary """
# d.get(key, [default])

print('DB open')
# print(ip_address['unknown'])    # abruptly terminates code execution
print(ip_address.get('unknown'))  # if key doesn't exist, returns None
print('subnet IP:', ip_address.get('subnet'))  # if key doesn't exist, returns None
print('DB close')

# result = ip_address.get('unknown', 'Key not found')
# result = ip_address.get('incorrect', 'unable to proceed')

# # How will you add an empty list to the dictionary if its key is not found?

# result = ip_address.get('assigned', [])  # when key is not there return a empty list
# ip_address['assigned'] = result     # one item assignment

# ip_address['assigned'] = ip_address.get('assigned', [])
# ip_address['dns'] = ip_address.get('dns', [])

# print('assigned ip:', result)
pprint(ip_address)

# d.update(dict)
# - It updates multiple key - value pairs(similar to extend() in list)
# - Adds items if doesn't exists, else updates the existing one

ip_address.update({'localhost': 'localhost', 'dhcp': []})
pprint(ip_address)

ip_address.get('dhcp').append('10.197.0.85')
pprint(ip_address)

# d.pop(key)
#     - it removes the item(key, value) from a dictionary
#     - it returns the removedkey 's value
# print(hello)
del ip_address  # deleting the ip tables totally
print(ip_address)

a = 10
del a
a += 1

# How to delete an item in a dictionary?

l = [1, 2, 3, 'swad', 'hari']
print(l)
del l[-1]
print(l)
del l[0]
print(l)
del l
print(l)

del ip_address['localhost']
pprint(ip_address)

del ip_address['dhcp']
popped_item = ip_address.pop('dhcp')

# What are the ways to delete an item in a dict?
# del
# dict.pop(key)

# # List
# del l[index]
# list.remove()

# # Tuple
# A tuple cannot be altered
# del tuple_obj

popped_value = ip_address.pop('subnet')  # pop - remove as well as return the value
pprint(ip_address)
pprint(popped_value)

# # How to delete an item if it exists?
# del ip_address['inexistent']

if 'default_gw' in ip_address:
    popped_item = ip_address.pop('default_gw')  # KeyError: 'wrong_key'
    pprint(ip_address)
    print(f'The value removed was: {popped_item}')
else:
    print('unable to find key: wrong_key')
