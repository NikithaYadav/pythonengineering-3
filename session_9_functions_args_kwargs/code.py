"""
    Function definitions with variable no. of arguments:
        3. *args     -  tuple
        4. **kwargs  -  dictionary
"""


# def f(a1, a2, d1=1, d2=3):  # 4 no. of args
#     print(a1, a2, d1, d2)
# f(0, 1, 0, 0, 10, 20)

def average(s1, s2):
    print(s1, s2)
    sum = s1 + s2
    avg = sum / 2
    return avg


# >>> a, *tmp, e = 10, 20, 30, 40, 50
# >>> a
# 10
# >>> e
# 50
# >>> tmp
# [20, 30, 40]

avg = average(10, 20)


# avg = average(s2=10, s1=20, s3=10)


def average_marks(student_name, *scores):  # function accepts any number of arguments
    total_scores = sum(scores)
    total_subjects = len(scores)
    avg = total_scores / total_subjects
    print(f'Average marks of "{student_name}": {avg:.2f}')
    print(f'Average rounded avg marks of "{student_name}": {round(avg)}')


average_marks('swadhi', 50, 60, 57, 66, 54, 60)


def append_contents(*contents, separator=','):
    """
    Appends each item in the arguments and returns as string

    :param contents:    any basic data type (int, str, float)
    :param separator:   separates each content while printing
    :return: str        appended string
    """
    result_str = ''
    for char in contents:
        result_str = result_str + str(char) + separator
    return result_str


# result = append_contents()
result = append_contents('swadhi', 'chiru', 'govind')
print(result)
# result = append_contents(100, 200, 300)  # TypeError: can only concatenate str (not "int") to str
result = append_contents(1.1, 2.2, 3.3, separator=':::')  # TypeError: can only concatenate str (not "int") to str
print(result)
print(type(result))


def f(arg1=0):  # restricted to only one arg
    pass


def score_board(**kwargs):
    print(kwargs)
    print(type(kwargs))
    for k, v in kwargs.items():
        if v < 60:
            print(f'{k} has scored poor marks: {v}')
        print(k, v, sep='==')


# f()
# f(arg1=1, arg2=2, arg3=3)   # caller
swadhi_mark = 56
brindha_mark = 67
govind_mark = 77
chiru_mark = 65
score_board(swadhi=swadhi_mark, brindha=brindha_mark, govind=govind_mark, chiru=chiru_mark)

def visiting_card(**address):
    """
    Brindha Srinivasan
    23, West Street,
    Chennai - 500234,
    India
    """
    # pprint.pprint(address)

    if 'name' not in address or 'door' not in address \
            or 'street' not in address or 'city' not in address or \
            'pin' not in address:
        print(f'Error: All the below params are required:\nname, door, pin, city, street')
        print('Program exiting ...')
        return

    card = f"""
        {address['name']},
        {address['door']}, {address['street']},
        {address['city']} - {address['pin']},
        India
    """
    print(card)


visiting_card(
    name='Brindha Srinivasan',
    door=23,
    street='West Street',
    city='Chennai',
    pin=500234
)

visiting_card(
    name='Govind Kaluva',
    door=49,
    street='Yellow avenue',
    city='Tirupati',
    pin=40020
)

karmas = {
    'swad': 0,
    'gov': 0,
    'brin': 0,
    'chir': 0
}


def karma_bot(**karma_dict):
    for user, karma_string in karma_dict.items():
        if '+' in karma_string:
            karmas[user] = karmas[user] + len(karma_string)
            print(f"{user}'s karma is increased to: {karmas[user]}")

        if '-' in karma_string:
            karmas[user] = karmas[user] - len(karma_string)
            print(f"{user}'s karma is decreased to: {karmas[user]}")


# One user:
# def karma_bot(user, karma_string):
#     if '+' in karma_string:
#         karmas[user] = karmas[user] + len(karma_string)
#         print(f"{user}'s karma is increased to: {karmas[user]}")
#
#     if '-' in karma_string:
#         karmas[user] = karmas[user] - len(karma_string)
#         print(f"{user}'s karma is decreased to: {karmas[user]}")


karma_bot(swad='+++++', brin='---', gov='+++++', chir='+++++++++')
karma_bot(chir='+++++++++')

# karma_bot('swad', '+++++')
# karma_bot('swad', '----------------')
# karma_bot('swad', '')
