Installations:

Python:
  Python 3.7 Download and install
  https://www.python.org/ftp/python/3.7.4/python-3.7.4.exe

  CHeck version in cmd:
  C:\WINDOWS\system32>python --version
  Python 3.7.4

Pycharm:
  Install Pycharm Community edition
  https://www.jetbrains.com/pycharm/download/#section=windows

  Setup python interpreter: #  What is thi interpreter 
  File -> Settings -> Project (automation_engineers) -> Add


Bitbucket:
  Create Bitbucket account to collaborate to "PythonEngineering" repository

  Repo url:
  https://neoswa@bitbucket.org/neoswa/pythonengineering.git


Git push and pull steps:
  Git commit your local work
  Git pull (brings code from remote repo)
  Git push (pushes your changes to bitbucket)