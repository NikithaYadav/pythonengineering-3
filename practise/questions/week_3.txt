1. Dictionary looping and builtin functions:

Given the below dictionary:
teams = {'Colorado': 'Rockies', 'Boston': 'Red Sox', 'Minnesota': 'Twins', 'Milwaukee': 'Brewers', 'Seattle': 'Mariners'}

1. Write a simple for loop that:
     a. prints only the keys
     b. prints only the values
     c. prints the keys and values in format:
            key1~value1
            key2~value2

Given the below dictionary:
family = {'fname': 'Joe', 'lname': 'Fonebone', 'age': 51, 'spouse': 'Edna', 'children': ['Ralph', 'Betty', 'Joey'], 'pets': {'dog': 'Fido', 'cat': 'Sox'}}

2. Write a simple for loop that prints.
   a. each child's name in a separate line
   b. each pet's name in format:
        "dog's name is ..."
        "cat's name is ..."
   c. Now add another pet of type rabbit with name bunny and pretty print the dictionary
   d. Now update the dict in qn. 1 to this dictionary with key as 'teams'


Given the below dictionary
d = {0: 'a', 1: 'a', 2: 'a', 3: 'a'}

3. Using for loop print the sum of all the keys

4. Use a for loop to append all the values into a single string

5. What is the purpose of dict.clear() method? Give example.

6. Empty all the above dictionaries and print of them. THe contents should be empty.

7. Now, delete all the above dictionaries totally from the memory.

Searching items:

random_list =  [21.42, 'foobar', 3, 4, 'bark', False, 3.14159]

Given the above list,

8.  Search and print True or False, if 5 exists in the list
9.  Search and print True or False, if any number between 20 to 25 exists in the list
10. Search and print if any string which has a substring of foo
11. Search and print any boolean value
12. Search and print any number that is a perfect square. [google to find how a number is a perfect square]


Functions
Write a function with name "get_employee_badge"
      1. Arguments: 3 mandatory arguments - emp_id, first_name, last_name,
      2. Inside the function check if the emp_id parameter is an integer:
              if not, then print "Invalid employee id: <emp_id>"
              else, print a string that looks like: '<emp_id> : <first_name>:<last_name>'

Sample solution:
def get_employee_badge(emp_id, first_name, last_name):
    if type(emp_id) != int:
        print('Invalid employee id:', emp_id)
    else:
        print(emp_id, first_name, last_name, sep=':')


get_employee_badge('wrong', 'swadhikar', 'chandramohan')  Invalid employee id: wrong
get_employee_badge(423367, 'swadhikar', 'chandramohan')  423367:swadhikar:chandramohan


13. Write a function with name "get_mail_id"
      1. Arguments: 3 mandatory arguments - first_name, last_name, company_name
      2. Inside the function check if the company_name parameter is a string whose size is lesser than or equals 5:
              if the size is greater than 5, then print "Company acronym can have a max size of 5 characters"
              else, return a mail id of format: <firstname>.<lastname>@<company>.com

14 . Write a function with name "get_emp_grade"
      1. Arguments:
                      2 mandatory arguments - "emp_id", "firstname"
                      1 default argument    - "experience" with initial value of 0
      2. Inside the function,
              if the experience is lesser than 1, then print "<emp_id> is a fresher!"
              if the experience is greater than 1 and lesser than 3, then return "<emp_id> is a junior developer"
              if the experience is greater than 3, then return "<emp_id> is a senior developer"
      3. Make three kinds of calls to this function:
              call 1 - do not pass the parameter "experience" and print the result
              call 2 - pass the parameter "experience" lesser than 3 and print the result
              call 1 - the parameter "experience" greater than 3 and print the result


15. Write a function with name "get_circle_volume"
      1. Arguments:
                      1 default argument - "radius" with default value 0
      2. Inside the function,
              if the radius is lesser than 0 or equal to 0, then return 0,
              else return the circle volume (volume formula: (4/3) * (22/7) * r ** 3)
      3. Make three kinds of calls to this function:
              call 1 - do not pass the parameter "radius" and print the result
              call 2 - pass the parameter "radius" lesser than 0 and print the result
              call 1 - the parameter "radius" greater than 0 and print the result


16. Write a function with any name that accepts one default argument at first
    followed by a mandatory argument and note the result. (hint: f(d=10, m) )

17. Write a function with any name that accepts one default argument at first
    followed by a mandatory argument and note the result. (hint: f(d=10, m) )

Sample question for *args:
Write a function that accepts any number of integer arguments and returns the product of all its numbers
def get_product(*numbers):
    product = 1
    for number in numbers:
        product = product * number
    return product

18. Write a function that accepts any number of integer arguments and returns the difference of all its numbers

19. Write a function that:
      a. Accepts one mandatory argument - student_id
      b. Any number of marks as *marks
   Inside the function check:
      c. If *marks has any content, then calculate the sum of all marks and return total marks of a student
      d. Else, return 0

20. Write a function that has any number of keyword arguments (kwargs) that has keys
   as student ids in a classroom and his/her marks as value. For example, 'Student'=1800, 'Student2'=900
   The function should return the total marks scored by all students in the class

21. Write a function that has:
      Arguments:
          a. party name (mandatory)
          b. variable number of keyword arguments with district names as keys and votes secured as values
                  For example: chennai=18000, trichy=7800 and so on.
      The function should calculate the total votes and print "<partyname> has got a total of <totalvotes> votes'

22. Write a function that has:
      Arguments:
          a. party name (mandatory)
          b. variable number of keyword arguments with district names as keys and votes secured as values
              For example: chennai=18000, trichy=7800 and so on.
#
      The function should:
          1. Check if the **kwargs has any content in it. If not, then simply return the function
          2. else, calculate the total votes and print "<partyname> has got a total of <totalvotes> votes'

